import React, { useState } from "react";
import { Buscador } from "../components/Buscador";
import { Contador } from "../components/Contador";
import { Header } from "../components/Header";
import { Titulo } from "../components/Titulo";
import { Notas } from "../components/Notas";
function Inicial() {
  const [data, setData] = useState(JSON.parse(localStorage.getItem("notas")));
  console.log(data);
  return (
    <div>
      <div className="d-flex p-3 justify-content-end">
        <Header />
      </div>
      <div className="d-flex p-2 justify-content-center">
        <Titulo />
      </div>
      <div className="d-flex justify-content-center m-3">
        <Contador />
      </div>
      <div className="container">
        <Buscador />
      </div>
      <div className="container">
        {data != null
          ? Object.values(data).map((value, index) => {
              if (value.status > 0) {
                return (
                  <Notas key={index} data={{ value: value, key: index }} />
                );
              } else {
                return null;
              }
            })
          : null}
      </div>
    </div>
  );
}

export { Inicial };
