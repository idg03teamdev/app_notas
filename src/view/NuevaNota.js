import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function NuevaNota() {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [date, setDate] = useState("");

  function today() {
    let n = new Date();
    //Año
    let y = n.getFullYear();
    //Mes
    let m = n.getMonth() + 1;
    //Día
    let d = n.getDate();
    setDate(y + "-" + m + "-" + d);
  }

  useEffect(() => {
    today();
  });

  function makeid() {
    var res = 0;
    var char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var size = char.length;
    for (let index = 0; index < 12; index++) {
      res += char.charAt(Math.floor(Math.random() * size));
    }
    return res;
  }

  function guardarnota() {
    //localStorage.clear();

    let notestotal = [];
    if (title === "" || desc === "" || date === "") {
      alert("faltan datos");
    } else {
      let num = makeid();
      let note = {
        id: num,
        titulo: title,
        desc: desc,
        fecha: date,
        status: 1,
      };

      let notes = [note];

      console.log("note llenar :", notes);

      let notecache = JSON.parse(localStorage.getItem("notas"));

      console.log("notecache", notecache);
      console.log();
      if (notecache === null) {
        console.log("es nullo");

        localStorage.setItem("notas", JSON.stringify(notes));
      } else {
        for (let i = 0; i < notecache.length + 1; i++) {
          if (notecache.length === i) {
            console.log("a subir ", notes[0]);

            notestotal.push(notes[0]);
          } else {
            console.log("en cache ", notecache[i]);
            notestotal.push(notecache[i]);
          }
        }

        localStorage.setItem("notas", JSON.stringify(notestotal));
      }
      console.log("notestotal", notestotal);

      console.log("en local :", JSON.parse(localStorage.getItem("notas")));
    }
  }

  return (
    <div>
      <div className="p-3 d-flex justify-content-end">
        <nav>
          <div>
            <Link className="btn btn-success btn-circle" to="/">
              Regresar
            </Link>
          </div>
        </nav>
      </div>

      <form className="container-fluid p-3">
        <div className="mb-3">
          <label className="form-label">Titulo</label>
          <input
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Titulo"
            className="form-control"
            type="text"
          />
        </div>

        <div className="mb-3">
          <label className="form-label">Descripción</label>
          <textarea
            onChange={(e) => setDesc(e.target.value)}
            placeholder="Descripción"
            className="form-control"
            type="text"
          ></textarea>
        </div>

        <div className="mb-3">
          <label className="form-label">Fecha</label>
          <input
            onChange={(e) => setDate(e.target.value)}
            placeholder="Fecha"
            value={date}
            className="form-control"
            type="date"
          />
        </div>

        <div className="mb-3">
          <button
            type="button"
            onClick={guardarnota}
            className="btn btn-success form-control"
          >
            Guardar
          </button>
        </div>
      </form>
    </div>
  );
}

export { NuevaNota };
