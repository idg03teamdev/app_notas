import React from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";

import "./App.css";

import { Inicial } from "./view/Inicial";
import { NuevaNota } from "./view/NuevaNota";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Inicial />} />
        <Route path="/nuevaNota" element={<NuevaNota />} />
      </Routes>
    </Router>
    // <div className="App">
    //   <header className="App-header"></header>
    // </div>
  );
}

export default App;
