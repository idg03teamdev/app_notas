import React from "react";

function Buscador() {
  return (
    <form>
      <div className="input-group mb-3">
        <span className="input-group-text">Nota</span>
        <input type="text" className="form-control" placeholder="Buscar..." />
      </div>
    </form>
  );
}
export { Buscador };
