import React, { useState, useEffect } from "react";

function Contador() {
  const [local, setLocal] = useState("");
  const [sizelocal, setSizelocal] = useState("");
  const [sizetotal, setSizetotal] = useState(0);
  const [texto, setTexto] = useState("");

  useEffect(() => {
    let contar = 0;
    let localData = JSON.parse(localStorage.getItem("notas"));
    if (localData != null) {
      //setSizelocal(localData.length);
      let contartrue = 0;
      for (let index = 0; index < localData.length; index++) {
        const element = localData[index];
        if (element.status == 2) {
          contar++;
          setSizetotal(contar);
        }
        if (element.status == 1) {
          contartrue++;
          setSizelocal(contartrue);
        }
        console.log("size : ", sizetotal);
      }
    } else {
      setSizelocal("");
    }

    setTimeout(() => {
      if (sizelocal > 0) {
        setTexto(`Tareas realizadas : ${sizetotal} de ${sizelocal}`);
      } else {
        setTexto("Aun no tienes notas ");
      }
    }, 100);
  }, [sizelocal]);

  return <h1>{texto}</h1>;
}

export { Contador };
