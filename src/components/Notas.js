import React, { useEffect, useState } from "react";
import "./css/Notas.css";
function Notas(props) {
  const [id, setId] = useState(props.data.value.id);
  const [titulo, setTitulo] = useState(props.data.value.titulo);
  const [desc, setDesc] = useState(props.data.value.desc);
  const [fecha, setFecha] = useState(props.data.value.fecha);
  const [status, setStatus] = useState(props.data.value.status);

  let search = JSON.parse(localStorage.getItem("notas"));
  const [localcache, setLocal] = useState(search[props.data.key]); //search[props.data.key]

  function eliminar() {
    console.log(status, id);
    let notestotal = [];

    console.log("search", search);
    let arrayfind = search.find((sr) => sr.id === id);
    console.log(arrayfind);

    let responsemap = mapeo(0);
    console.log("responsemap", responsemap);

    console.log("searcho >>>", search);
    localStorage.clear();

    localStorage.setItem("notas", JSON.stringify(search));

    console.log("en local :", JSON.parse(localStorage.getItem("notas")));
  }

  function check() {
    console.log(status, id);
    let notestotal = [];

    console.log("search", search);
    let arrayfind = search.find((sr) => sr.id === id);
    console.log(arrayfind);

    let responsemap = mapeo(2);
    console.log("responsemap", responsemap);

    console.log("searcho >>>", search);
    localStorage.clear();

    localStorage.setItem("notas", JSON.stringify(search));

    console.log("en local :", JSON.parse(localStorage.getItem("notas")));
  }

  function mapeo(numstatus) {
    search.map(function(dato) {
      if (dato.id == id) {
        console.log("son igual");
        dato.status = numstatus;
      }
      console.log("dato", dato);
      return dato;
    });
  }

  return (
    <div className="col-md-12">
      <div className="card card-body">
        <h5 className="note-title text-truncate w-75 mb-0">{titulo}</h5>
        <p className="note-date font-12 text-muted">{fecha}</p>
        <div className="note-content">
          <p className="note-inner-content text-muted">{desc}</p>
        </div>
        <div className="d-grid gap-2 d-md-flex justify-content-md-start">
          <span className="mr-1">
            <button className="btn btn-light" onClick={check}>
              <i className="fa-regular fa-hourglass-half"></i>
            </button>
          </span>
          <span className="mr-1">
            <button className="btn btn-outline-danger" onClick={eliminar}>
              <i className="fa-regular fa-trash-can"></i>
            </button>
          </span>
        </div>
      </div>
    </div>
  );
}
export { Notas };
