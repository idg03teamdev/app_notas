import React from "react";
import { Link } from "react-router-dom";
import "./css/Header.css";
function Header() {
  return (
    <nav>
      <div className="container">
        <Link className="btn btn-success btn-circle" to="/nuevaNota">
          Nota
        </Link>
      </div>
    </nav>
  );
}

export { Header };
