const CACHE_ESTATICO = "static-v1";
const CACHE_DINAMICO = "dinamic-v1";
const CACHE_INMUTABLE = "inmutable-v1";

const APP_SHELL = [
  "/",
  ".index.html",
  "/src/app.js",
  "/manifest.json",
  "/sw.js",
  "/src/bootstrap/css/bootstrap.min.css",
  "/src/bootstrap/js/bootstrap.bundle.min.js",
];

const APP_SHELL_INMUTABLE = [
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/brands.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/fontawesome.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/regular.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/solid.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/svg-with-js.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/v5-font-face.min.css",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/brands.min.js",
  // "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/conflict-detection.min.js",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/fontawesome.min.js",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/regular.min.js",
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/solid.min.js",
];

function limpiarCache(cacheName, numeroItem) {
  caches.open(cacheName).then((cache) => {
    return cache.keys().then((keys) => {
      if (keys.length > numeroItem) {
        cache.delete(keys[0]).then(limpiarCache(cacheName, numeroItem));
      }
    });
  });
}

self.addEventListener("install", (event) => {
  const cacheStatico = caches.open(CACHE_ESTATICO).then((cache) => {
    return cache.addAll(APP_SHELL);
  });
  const cacheInmutable = caches.open(CACHE_INMUTABLE).then((cache) => {
    return cache.addAll(APP_SHELL_INMUTABLE);
  });
  event.waitUntil(Promise.all([cacheStatico, cacheInmutable]));
});

self.addEventListener("activate", function(event) {
  event.waitUntil(self.clients.claim());
});
self.addEventListener("fetch", (event) => {
  const respuesta = caches.match(event.request).then((res) => {
    if (res) return res;
    //No existe- ir a la web
    console.log("No existe", event.request.url);

    return fetch(event.request).then((newResp) => {
      caches.open(CACHE_DINAMICO).then((cache) => {
        cache.put(event.request, newResp);
        limpiarCache(CACHE_DINAMICO, 5);
      });

      return newResp.clone();
    });
  });
  event.respondWith(respuesta);
});
